#include <boost/units/quantity.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si/codata/electron_constants.hpp>
#include <vector>
#include <chrono>
#include <regex>
#include <unistd.h>

#include <CLI/CLI.hpp>

#include <betaboltz.hpp>

using namespace dfpe;
using namespace boost::units;
using namespace boost::filesystem;
using namespace std;
using namespace CLI;

static string RED = "\u001B[31m";
static string BLU = "\u001B[34m";
static string GRY = "\u001B[37m";
static string RST = "\u001B[0m";




size_t correctW(const string& s)
{
    size_t count = 0;
    for (auto p = s.begin(); p != s.end(); ++p)
        count += ((*p & 0xc0) != 0x80);
    return s.length() - count;
}

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}


void print_head()
{
    cout << RED
        << std::left << std::setw(30) << "FIELD"
        << std::setw(8) << "SYMBOL"
        << std::right << std::setw(12) << "VALUE" << " "
        << std::left << std::setw(8) << "" << "  "
        << std::right << std::setw(12) << "STDEV"  << " "
        << std::left << std::setw(8) << ""
        << RST;

    cout << endl;

}

template<typename T>
void print(const string& label, const string& symbol, T mean, optional<T> stdev, T unitValue, const string unitSymbol)
{
    cout
        << std::left << std::setw(30 + correctW(label)) << label
        << BLU << std::setw(8 + correctW(symbol)) << symbol << RST
        << std::right << std::setw(12) << (double) (mean / unitValue) << " "
        << GRY << std::left << std::setw(8 + correctW(unitSymbol)) << unitSymbol << RST;

    if (stdev.has_value())
        cout << " ±" << std::right << std::setw(12) << (double) (stdev.value() / unitValue)  << " "
             << GRY << std::left << std::setw(8 + correctW(unitSymbol)) << unitSymbol << RST;

    cout << endl;

}

void print(const string& label, const string& symbol, int value)
{
    cout << std::left << std::setw(30 + correctW(label)) << label << std::setw(8 + correctW(symbol)) << symbol << std::right << std::setw(12) << value << std::left << endl;
}

int main(int argc, char** argv)
{

    if (!isatty(fileno(stdout)))
    {
        // removing ANSI codes when redirecting output to something that is not a terminal
        RED = "";
        BLU = "";
        GRY = "";
        RST = "";
    }

    CLI::App app{"Drifter - Gas Drift Velocity"};

    vector<string> gases;
    app.add_option("-g,--gases", gases, "Gas molecules")->required();

    vector<string> databases;
    app.add_option("-d,--databases", databases, "Cross-section databases: it is possible to specify the database names (case insensitive), like 'Biagi' or a selection filters like 'Biagi/Ar/1,46' or 'Itikawa/O2/4-9'  (case sensitive) .")->required();

    vector<double> ratios = {100.};
    app.add_option("-x,--ratios", ratios, "Gas ratios")->capture_default_str();;

    optional<double> electricFieldDouble;
    optional<double> electricFieldRedDouble;

    auto& optionGroupField  = *app.add_option_group("field", "Electric field in the gas");

    auto optionField        = optionGroupField.add_option("--field", electricFieldDouble, "Electric field [kV/cm]");
    auto optionReducedField = optionGroupField.add_option("--reduced-field", electricFieldRedDouble, "Reduced electric field [Td]");

    double magneticFieldValueDouble = 0.;
    app.add_option("--magnetic-field", magneticFieldValueDouble, "Magnetic field [T]");

    double magneticFieldAngleDouble = 0.;
    app.add_option("--magnetic-angle", magneticFieldAngleDouble, "Magnetic angle [deg]");

    optionGroupField.required();

    optionField->excludes(optionReducedField);
    optionReducedField->excludes(optionField);

    int runId = 0;
    app.add_option("-r,--run", runId, "Run id")->capture_default_str();

    double pressureDouble = 101.325;
    app.add_option("-p, --pressure", pressureDouble, "Gas pressures [kPa]")->capture_default_str();

    double temperatureDouble = 293.15;
    app.add_option("-t, --temperature", temperatureDouble, "Gas temperature [K]")->check(PositiveNumber)->capture_default_str();

    int events = 1;
    app.add_option("-e,--events", events, "Events")->check(PositiveNumber)->capture_default_str();

    int particles = 25;
    app.add_option("-n,--particles", particles, "Particles per event")->check(PositiveNumber)->capture_default_str();

    optional<int> jobs;
    app.add_option("-j,--jobs", jobs, "Number of threads")->check(PositiveNumber)->capture_default_str();

    int interactions = 200000;
    app.add_option("-i,--interactions", interactions, "Number of interactions per event.")->check(NonNegativeNumber)->capture_default_str();

    bool relativistic = false;
    app.add_flag("--relativistic,!--classic", relativistic, "Simulation mode")->capture_default_str();

    string trialAlgo = "variable";
    app.add_option("--trial-algo", trialAlgo,  "Trial frequency algorithm (allowed: lazy, variable, fixed(...), binned(...)")->capture_default_str();

    string scatteringLabel = "okhrimovskyy";
    app.add_option("-s,--scattering", scatteringLabel, "Scattering algorithm to use for elastic collisions")->check(IsMember(ScatteringNameUtil::getAllNames()))->capture_default_str();

    bool verbose = false;
    app.add_flag("-v,--verbose", verbose, "Verbose mode");

    bool saveAppend = false;
    app.add_flag("-a,--append", saveAppend, "Append path instead of overwrite")->capture_default_str();

    std::optional<string> saveCollisionsPath;
    app.add_flag("--save-collisions{collisions.csv}", saveCollisionsPath, "Save collisions to filename")->expected(0,1);
    std::optional<string> saveStepsPath;
    app.add_flag("--save-steps{steps.csv}", saveStepsPath, "Save steps to filename")->expected(0,1);
    std::optional<string> saveFrequenciesPath;
    app.add_flag("--save-frequencies{frequencies.csv}", saveFrequenciesPath, "Save total interaction frequencies to filename")->expected(0,1);
    std::optional<string> saveAllPrefix;
    app.add_flag("--save-all{}", saveAllPrefix, "Save all csv with a given prefix")->expected(0,1);

    optional<string> output;
    app.add_option("-o,--output", output, "Output file");

    bool showTables = false;
    app.add_flag("--show-tables", showTables, "Show the cross-section tables selected");

    bool disableTemperature = false;
    app.add_flag("--disable-temperature", disableTemperature, "Ignore target gas thermal motion");

    bool disableXiTable = false;
    app.add_flag("--disable-xi-table", disableXiTable, "Disable Xi table");

    bool disableCheckPolar = false;
    app.add_flag("--disable-check-polar", disableCheckPolar, "Override polar molecule check");

    bool disableCheckMonoatomic = false;
    app.add_flag("--disable-check-monoatomic", disableCheckMonoatomic, "Override monatomic molecule check");

    try
    {
        app.parse(argc, argv);
    }
    catch (const CLI::ParseError &e)
    {
        return app.exit(e);
    }

    auto timeStart = std::chrono::high_resolution_clock::now();

    QtySiPressure                     unitPressure                          (1.    * si::kilo * si::pascal);                         // kPa
    QtySiTemperature                  unitTemperature                       (1.    * si::kelvin);                                    // K
    QtySiDensity                      unitDensity                           (1.    / (si::centi * si::meter * si::centi * si::meter * si::centi * si::meter)); // 1/cm³
    QtySiElectricField                unitField                             (1.    * si::kilo * si::volt / (si::centi * si::meter)); // kV/cm
    QtySiReducedElectricField         unitReducedField                      (1e-21 * si::volt * si::meter * si::meter);              // Td
    QtySiMagneticField                unitMagneticField                     (1.    * si::tesla);                                     // T
    QtySiVelocity                     unitVelocity                          (1.    * si::centi * si::meter / (si::micro * si::second)); // cm/us
    QtySiPlaneAngle                   unitPlaneAngle                        (1.    * degree::degree);                                   // deg
    QtySiLength                       unitLength                            (1.    * si::centi * si::meter);                            // cm
    QtySiArea                         unitArea                              (1.    * si::centi * si::meter * si::centi * si::meter);    // cm²
    QtySiWavenumber                   unitWavenumber                        (1.    / (si::centi * si::meter));                       // 1/cm
    QtySiEnergy                       unitEnergy                            (1.602176634e-19    *  si::joule);                       // eV
    QtySiNormalizedElectricalMobility unitNormalizedElectricalMobility      (1. / (si::centi * si::meter) / si::volt / si::second); // /(cm⋅V⋅s)
    QtySiElectricalMobility           unitElectricalMobility                (1. * si::centi * si::meter * si::centi * si::meter / si::volt / si::second); // cm²/(V⋅s)
    QtySiDiffusion                    unitDiffusion                         (1. * si::centi * si::meter * si::centi * si::meter / si::second); // cm²/s
    QtySiNormalizedDiffusion          unitNormalizedDiffusion               (1. / (si::centi * si::meter) / si::second); // 1/(cm⋅s)
    QtySiElectricPotential            unitElectricPotential                 (1. * si::volt); // V

    auto kB = boost::units::si::constants::codata::k_B;
    QtySiDensity loschmidt(2.6867811e25 / si::cubic_meter);

    QtySiPressure             pressure = pressureDouble * unitPressure;
    QtySiTemperature          temperature = temperatureDouble * unitTemperature;
    QtySiDensity              density =  pressure / kB / temperature;

    QtySiElectricField          electricField;
    QtySiReducedElectricField   electricFieldRed;

    if (electricFieldRedDouble.has_value())
    {
        electricField              = *electricFieldRedDouble * unitReducedField  * density;
        electricFieldRed       = *electricFieldRedDouble * unitReducedField;
    }
    else if (electricFieldDouble.has_value())
    {
        electricField              = *electricFieldDouble * unitField;
        electricFieldRed       = *electricFieldDouble * unitField / density;
    }

    QtySiMagneticField          magneticField = magneticFieldValueDouble * unitMagneticField;
    QtySiPlaneAngle             magneticAngle = magneticFieldAngleDouble * unitPlaneAngle;

    regex trialAlgoRegex(R"(([a-z0-9]+)\s*(?:\((.*)\))?)");

//    QtySiFrequency trialValue(trialValueDouble * si::tera * si::hertz);

    //if (interactions == 0)
    //    interactions = max(min((int)(500000. * unitReducedField / electricFieldRed), 10000000), 100000);

    BetaboltzSimple beta;

    if (jobs.has_value())
        beta.setNumThreads(*jobs);

    BetaboltzFlags flags;
    flags.setDisableXiTable(disableXiTable);
    flags.setDisableCheckPolar(disableCheckPolar);
    flags.setDisableCheckMonoatomic(disableCheckMonoatomic);
    flags.setDisableTemperature(disableTemperature);

    flags.setEnableIonizations(false);
    flags.setEnableAttachments(false);

    beta.setFlags(flags);

    // Here we describe the gas mixturea
    vector<QtySiDensity> componentDensities;

    if (gases.size() == 1)
    {
        componentDensities.push_back(density);
    }
    else
    {
        if (ratios.size() != gases.size())
        {
            throw invalid_argument("--ratios count must match with --gases count");
        }

        double ratiosSum = std::accumulate(ratios.begin(), ratios.end(), 0.);

        for (double& ratio: ratios)
            componentDensities.push_back(density * ratio / ratiosSum);
    }

    if (databases.size() != 1 && databases.size() != gases.size())
    {
        throw invalid_argument("--databases count must match with --gases count (or must have just an element)");
    }

    GasMixture mixture;

    if (disableTemperature)
        temperature = QtySiTemperature();

    mixture.setTemperature(temperature);


    ScatteringName scatteringName = ScatteringNameUtil::parse(scatteringLabel);

    for (vector<string>::size_type i = 0; i < gases.size(); ++i)
    {
        const string& molecule = gases[i];
        QtySiDensity componentDensity = componentDensities[i];
        const string& database = databases.size() == 1 ? databases.at(0) : databases.at(i);

        mixture.addComponent(molecule, componentDensity);
        beta.enableProcess("e", molecule, database, scatteringName);
    }

    auto swarmHandler = make_shared<SwarmHandler>();
    swarmHandler->setDirection(VectorC3D<QtySiLength>(0. * si::meter, 0. * si::meter, -1. * si::meter));
    beta.addHandler(swarmHandler);


    if (saveAllPrefix.has_value())
    {
        saveCollisionsPath  = saveAllPrefix.value() + "collisions.csv";
        saveStepsPath       = saveAllPrefix.value() + "steps.csv";
        saveFrequenciesPath = saveAllPrefix.value() + "frequencies.csv";
    }

    if (saveCollisionsPath.has_value())
    {
        auto exportCollisionsHandler = make_shared<ExportCollisionsHandler>(path(*saveCollisionsPath), saveAppend);
        beta.addHandler(exportCollisionsHandler);
    }

    if (saveStepsPath.has_value())
    {
        auto exportStepsHandler = make_shared<ExportStepsHandler>(path(*saveStepsPath), saveAppend);
        beta.addHandler(exportStepsHandler);
    }

    if (saveFrequenciesPath.has_value())
    {
        auto exportFrequenciesHandler = make_shared<ExportFrequenciesHandler>(path(*saveFrequenciesPath), saveAppend);
        beta.addHandler(exportFrequenciesHandler);
    }

    auto progressHandler = make_shared<PrintProgressHandler>(PrintProgressVerbosity::EVENT_LEVEL);
    if (verbose)
        beta.addHandler(progressHandler);

    auto printTablesHandler = make_shared<PrintTablesHandler>();
    if (showTables)
        beta.addHandler(printTablesHandler);

    auto limiter = make_shared<InteractionBulletLimiter>(interactions);
    beta.addLimiter(limiter);

    ElectronSpecie electronSpecie;

    VectorC3D<QtySiElectricField> electricFieldVector(QtySiElectricField(), QtySiElectricField(), electricField);
    VectorC3D<QtySiMagneticField> magneticFieldVector(QtySiMagneticField(), QtySiMagneticField(), magneticField);
    magneticFieldVector = magneticFieldVector.rotateY(magneticAngle);

    shared_ptr<BaseField> field;
    if (relativistic)
        field = make_shared<UniformFieldRelativisticChin>(electricFieldVector, magneticFieldVector);
    else
        field = make_shared<UniformFieldClassic>(electricFieldVector, magneticFieldVector);

    shared_ptr<BaseTrialFrequency> trialFrequency;
    string trialAlgoName;
    string trialAlgoParamsStr;

    smatch trialAlgoMatch;
    if (!regex_match (trialAlgo,trialAlgoMatch,trialAlgoRegex))
    {
        cerr << "ERROR: unable to parse trial algorithm '" << trialAlgo << "'" << endl;
        return -2;
    }

    trialAlgoName = trialAlgoMatch[1];

    if (trialAlgoMatch.size() >= 3)
        trialAlgoParamsStr = trialAlgoMatch[2];

    std::map<std::string,std::string> trialAlgoParams;

    std::istringstream iss(trialAlgoParamsStr);
    string token;
    while (std::getline(iss, token, ','))
    {
        trim(token);

        string key;
        string val;
        size_t pos = token.find_first_of(':');
        if (pos != string::npos)
        {
            key = token.substr(0, pos);
            val = token.substr(pos + 1);
        }
        else
        {
            key = token;
            val = "true";
        }

        trim(key);
        trim(val);

        trialAlgoParams[key] = val;
    }

    iss.clear();

    trim(trialAlgo);

    if (trialAlgoName == "lazy")
    {
        trialFrequency = make_shared<LazyTrialFrequency>();
    }
    else if (trialAlgoName == "variable")
    {
        auto algo = make_shared<VariableTrialFrequency>();

        if (auto it = trialAlgoParams.find("grace"); it != trialAlgoParams.end())
        {
            algo->setPeriod(stoi(it->second));
            trialAlgoParams.erase(it);
        }

        if (auto it = trialAlgoParams.find("overhead"); it != trialAlgoParams.end())
        {
            algo->setOverhead(stod(it->second));
            trialAlgoParams.erase(it);
        }

        trialFrequency = algo;
    }
    else if (trialAlgoName == "fixed")
    {
        if (auto it = trialAlgoParams.find("f"); it != trialAlgoParams.end())
        {
            auto trialFrequency = make_shared<FixedTrialFrequency>(stod(it->second) * QtySiFrequency(1. * si::tera * si::hertz));
            trialAlgoParams.erase(it);
        }
        else
        {
            cerr << "ERROR: 'fixed' trial algorithm should have the format 'fixed(F)' where F is the frequency in THz." << endl;
            return -2;
        }
    }
    else if (trialAlgoName == "binned")
    {
        auto algo = make_shared<BinnedTrialFrequency>();

        if (auto it = trialAlgoParams.find("grace"); it != trialAlgoParams.end())
        {
            algo->setPeriod(stoi(it->second));
            trialAlgoParams.erase(it);
        }

        if (auto it = trialAlgoParams.find("bins"); it != trialAlgoParams.end())
        {
            algo->setBins(stoi(it->second));
            trialAlgoParams.erase(it);
        }
        if (auto it = trialAlgoParams.find("emin"); it != trialAlgoParams.end())
        {
            algo->setEnergyMin(stod(it->second) * electronvolt);
            trialAlgoParams.erase(it);
        }

        if (auto it = trialAlgoParams.find("emax"); it != trialAlgoParams.end())
        {
            algo->setEnergyMax(stod(it->second) * electronvolt);
            trialAlgoParams.erase(it);
        }

        trialFrequency = algo;
    }
    else
    {
        cerr << "ERROR: invalid trial algo '" << trialAlgo << "'." << endl;
        return -2;
    }

    // Checking for leftover params

    if (!trialAlgoParams.empty())
    {
        stringstream ss;
        ss << "Unable to parse these arguments for '" + trialAlgoName + "' trial algorithm:";

        for (const auto& p: trialAlgoParams)
            ss << " " << p.first;

        throw invalid_argument(ss.str());
    }


    auto detector = make_shared<InfiniteDetector>(mixture, field);
    detector->setTrialFrequencyStrategy(trialFrequency);
    beta.setDetector(detector);

    beta.setNextRunId(runId);
    vector<ParticleState> initialStates(particles);

    try
    {
        beta.execute(electronSpecie, initialStates, events);
    }
    catch (const BetaboltzErrorCls& e)
    {
        cerr << "ERROR:" << endl;
        cerr << e.what() << endl;
        return 1;
    }

    auto timeEnd = std::chrono::high_resolution_clock::now();
    auto duration =  std::chrono::duration_cast<std::chrono::seconds>(timeEnd - timeStart).count();

    const pair<QtySiVelocity, QtySiVelocity>                &velocity            = swarmHandler->getDriftVelocity();
    const pair<QtySiWavenumber, QtySiWavenumber>            &townsendAlpha       = swarmHandler->getTownsendAlphaCoeff();
    const pair<QtySiDimensionless, QtySiDimensionless>      &townsendGamma       = swarmHandler->getTownsendGammaCoeff();
    const pair<QtySiWavenumber, QtySiWavenumber>            &townsendEta         = swarmHandler->getTownsendEtaCoeff();
    const pair<QtySiEnergy, QtySiEnergy>                    &energyBefore        = swarmHandler->getBulletPreEnergy();
    const pair<QtySiEnergy, QtySiEnergy>                    &energyAfter         = swarmHandler->getBulletPostEnergy();
    const pair<QtySiDiffusion, QtySiDiffusion>              &diffusionCoeff      = swarmHandler->getDiffusionCoeff();
    const pair<QtySiDiffusion, QtySiDiffusion>              &diffusionCoeffT     = swarmHandler->getDiffusionCoeffT();
    const pair<QtySiDiffusion, QtySiDiffusion>              &diffusionCoeffL     = swarmHandler->getDiffusionCoeffL();
    const pair<QtySiLength, QtySiLength>                    &diffusionMagnitude  = swarmHandler->getDiffusionMagnitude();
    const pair<QtySiLength, QtySiLength>                    &diffusionMagnitudeT = swarmHandler->getDiffusionMagnitudeT();
    const pair<QtySiLength, QtySiLength>                    &diffusionMagnitudeL = swarmHandler->getDiffusionMagnitudeL();

    const pair<QtySiElectricalMobility, QtySiElectricalMobility>    mobility (velocity.first / electricField, velocity.second / electricField);

    const pair<QtySiElectricPotential,  QtySiElectricPotential>     diffusionCoeffRatio (diffusionCoeff.first  / mobility.first,   hypot(diffusionCoeff.second  / diffusionCoeff.first,  mobility.second / mobility.first ) * diffusionCoeff.first  / mobility.first);
    const pair<QtySiElectricPotential,  QtySiElectricPotential>     diffusionCoeffRatioT(diffusionCoeffT.first / mobility.first,   hypot(diffusionCoeffT.second / diffusionCoeffT.first, mobility.second / mobility.first ) * diffusionCoeffT.first / mobility.first);
    const pair<QtySiElectricPotential,  QtySiElectricPotential>     diffusionCoeffRatioL(diffusionCoeffL.first / mobility.first,   hypot(diffusionCoeffL.second / diffusionCoeffL.first, mobility.second / mobility.first ) * diffusionCoeffL.first / mobility.first);

    auto stats = beta.getStatsEfficiency();

    if (output.has_value())
    {
        ofstream fileSim(*output, fstream::app);

        fpos pos = fileSim.tellp();

        if (pos <= 0)
            fileSim << "run_id, "
                       "events, "
                       "particles, "
                       "interactions, "
                       "electric_field [kV/cm], "
                       "electric_field_red [Td], "

                       "drift_velocity_mean [cm/µs], "
                       "drift_velocity_stdev [cm/µs], "
                       "mobility_mean [cm/(V s)], "
                       "mobility_stdev [cm/(V s)], "
                       "mobility_red_mean [cm/(V s)], "
                       "mobility_red_stdev [cm/(V s)], "
                       "mobility_norm_mean [1/(cm V s)], "
                       "mobility_norm_stdev [1/(cm V s)], "
                       "energy_before_mean [eV], "
                       "energy_before_stdev [eV], "
                       "energy_after_mean [eV], "
                       "energy_after_stdev [eV], "

                       "townsend_alpha_coeff_mean [cm^-1], "
                       "townsend_alpha_coeff_stdev [cm^-1], "
                       "townsend_alpha_coeff_red_mean [cm^2], "
                       "townsend_alpha_coeff_red_stdev [cm^2], "
                       "townsend_gamma_coeff_mean, "
                       "townsend_gamma_coeff_stdev, "
                       "townsend_eta_coeff_mean [cm^-1], "
                       "townsend_eta_coeff_stdev [cm^-1], "
                       "townsend_eta_coeff_red_mean [cm^2], "
                       "townsend_eta_coeff_red_stdev [cm^2], "

                       "diffusion_coeff_mean [cm^2/s], "
                       "diffusion_coeff_stdev [cm^2/s], "
                       "diffusion_coeff_norm_mean [1/(cm s)], "
                       "diffusion_coeff_norm_stdev [1/(cm s)], "
                       "diffusion_coeff_ratio_mean [V], "
                       "diffusion_coeff_ratio_stdev [V], "

                       "diffusion_coeff_long_mean [cm^2/s], "
                       "diffusion_coeff_long_stdev [cm^2/s], "
                       "diffusion_coeff_long_norm_mean [1/(cm s)], "
                       "diffusion_coeff_long_norm_stdev [1/(cm s)], "
                       "diffusion_coeff_long_ratio_mean [V], "
                       "diffusion_coeff_long_ratio_stdev [V], "

                       "diffusion_coeff_tran_mean [cm^2/s], "
                       "diffusion_coeff_tran_stdev [cm^2/s], "
                       "diffusion_coeff_tran_norm_mean [1/(cm s)], "
                       "diffusion_coeff_tran_norm_stdev [1/(cm s)], "
                       "diffusion_coeff_tran_ratio_mean [V], "
                       "diffusion_coeff_tran_ratio_stdev [V], "

                       "diffusion_magnitude_mean [cm], "
                       "diffusion_magnitude_stdev [cm], "
                       "diffusion_magnitude_tran_mean [cm], "
                       "diffusion_magnitude_tran_stdev [cm], "
                       "diffusion_magnitude_long_mean [cm], "
                       "diffusion_magnitude_long_stdev [cm], "

                       "real_collisions, "
                       "null_collisions, "
                       "fail_collisions, "
                       "efficiency [%], "
                       "elapsed_time [s]"
                       << endl;

        fileSim
                << runId << ", "
                << events << ", "
                << particles << ", "
                << interactions << ", "
                << (double) (electricField            / unitField ) << ", "
                << (double) (electricFieldRed     / unitReducedField ) << ", "

                << (double) (velocity.first        / unitVelocity ) << ", "
                << (double) (velocity.second       / unitVelocity ) << ", "
                << (double) (mobility.first        / unitElectricalMobility ) << ", "
                << (double) (mobility.second       / unitElectricalMobility ) << ", "
                << (double) (mobility.first  * density / loschmidt      / unitElectricalMobility ) << ", "
                << (double) (mobility.second * density / loschmidt      / unitElectricalMobility ) << ", "
                << (double) (mobility.first  * density                  / unitNormalizedElectricalMobility ) << ", "
                << (double) (mobility.second * density                  / unitNormalizedElectricalMobility ) << ", "
                << (double) (energyBefore.first          / unitEnergy     ) << ", "
                << (double) (energyBefore.second         / unitEnergy     ) << ", "
                << (double) (energyAfter.first          / unitEnergy     ) << ", "
                << (double) (energyAfter.second         / unitEnergy     ) << ", "

                << (double) (townsendAlpha.first   / unitWavenumber ) << ", "
                << (double) (townsendAlpha.second  / unitWavenumber ) << ", "
                << (double) (townsendAlpha.first   / density / unitArea ) << ", "
                << (double) (townsendAlpha.second  / density / unitArea ) << ", "
                << (double) (townsendGamma.first       ) << ", "
                << (double) (townsendGamma.second      ) << ", "
                << (double) (townsendEta.first     / unitWavenumber ) << ", "
                << (double) (townsendEta.second    / unitWavenumber ) << ", "
                << (double) (townsendEta.first     / density / unitArea ) << ", "
                << (double) (townsendEta.second    / density / unitArea ) << ", "

                << (double) (diffusionCoeff.first            / unitDiffusion             ) << ", "
                << (double) (diffusionCoeff.second           / unitDiffusion             ) << ", "
                << (double) (diffusionCoeff.first * density  / unitNormalizedDiffusion   ) << ", "
                << (double) (diffusionCoeff.second * density / unitNormalizedDiffusion   ) << ", "
                << (double) (diffusionCoeffRatio.first       / unitElectricPotential     ) << ", "
                << (double) (diffusionCoeffRatio.second      / unitElectricPotential     ) << ", "
                << (double) (diffusionCoeffL.first           / unitDiffusion             ) << ", "
                << (double) (diffusionCoeffL.second          / unitDiffusion             ) << ", "
                << (double) (diffusionCoeffL.first * density / unitNormalizedDiffusion   ) << ", "
                << (double) (diffusionCoeffL.second * density/ unitNormalizedDiffusion   ) << ", "
                << (double) (diffusionCoeffRatioL.first      / unitElectricPotential     ) << ", "
                << (double) (diffusionCoeffRatioL.second     / unitElectricPotential     ) << ", "
                << (double) (diffusionCoeffT.first           / unitDiffusion             ) << ", "
                << (double) (diffusionCoeffT.second          / unitDiffusion             ) << ", "
                << (double) (diffusionCoeffT.first * density / unitNormalizedDiffusion   ) << ", "
                << (double) (diffusionCoeffT.second * density/ unitNormalizedDiffusion   ) << ", "
                << (double) (diffusionCoeffRatioT.first      / unitElectricPotential     ) << ", "
                << (double) (diffusionCoeffRatioT.second     / unitElectricPotential     ) << ", "

                << (double) (diffusionMagnitude.first     / unitLength     ) << ", "
                << (double) (diffusionMagnitude.second    / unitLength     ) << ", "
                << (double) (diffusionMagnitudeL.first     / unitLength     ) << ", "
                << (double) (diffusionMagnitudeL.second    / unitLength     ) << ", "       
                << (double) (diffusionMagnitudeT.first     / unitLength     ) << ", "
                << (double) (diffusionMagnitudeT.second    / unitLength     ) << ", "

                << (double) (stats.realCollisions                                   ) << ", "
                << (double) (stats.nullCollisions                                   ) << ", "
                << (double) (stats.failCollisions                                   ) << ", "
                << (double) (stats.getEfficency() * 100.                            ) << ", "
                << (double) (QtySiTime (duration * si::second) / QtySiTime(1. * si::second))
                << endl;

        fileSim.close();
    }




    print_head();

    print("run_id"                  , ""    , runId);
    print("events"                  , ""    , events);
    print("particles"               , ""    , particles);
    print("interactions"            , ""    , interactions);
    print("gas pressure"            , "P"   , pressure, optional<QtySiPressure>(), unitPressure, "kPa");
    print("gas temperature"         , "T"   , temperature, optional<QtySiTemperature>(), unitTemperature, "K");
    print("gas density"             , "N"   , density, optional<QtySiDensity>(), unitDensity, "/cm³");
    print("electric field"          , "E"   , electricField, optional<QtySiElectricField>(), unitField, "kV/cm");
    print("electric field (red)"    , "E/N" , electricFieldRed, optional<QtySiReducedElectricField>(), unitReducedField, "Td");

    cout << endl;

    print("drift velocity"              , "W"   , velocity.first, optional<QtySiVelocity>(velocity.second), unitVelocity, "cm/μs");
    print("mobility"                    , "μ"   , mobility.first, optional<QtySiElectricalMobility>(mobility.second), unitElectricalMobility, "cm²/V⋅s");
    print("mobility (red)"              , "μ₀"  , mobility.first * density / loschmidt, optional<QtySiElectricalMobility>(mobility.second * density /loschmidt), unitElectricalMobility, "cm²/V⋅s");
    print("mobility (norm)"             , "μN"  , mobility.first * density, optional<QtySiNormalizedElectricalMobility>(mobility.second * density), unitNormalizedElectricalMobility, "/cm⋅V⋅s");
    print("energy before"                     , "ε"          , energyBefore.first, optional<QtySiEnergy>(energyBefore.second), unitEnergy, "eV");
    print("energy after"                      , "ε'"         , energyAfter.first,  optional<QtySiEnergy>(energyAfter.second),  unitEnergy, "eV");
    cout << endl;
    print("townsend alpha coeff"        , "α"   , townsendAlpha.first, optional<QtySiWavenumber>(townsendAlpha.second), unitWavenumber, "/cm");
    print("townsend alpha coeff (red)"  , "α/N" , townsendAlpha.first/density, optional<QtySiArea>(townsendAlpha.second/density), unitArea, "cm²");
    print("townsend gamma coeff"        , "γ"   , townsendGamma.first, optional<QtySiDimensionless>(townsendGamma.second), QtySiDimensionless(1.), "");
    print("townsend eta coeff"          , "η"   , townsendEta.first, optional<QtySiWavenumber>(townsendEta.second), unitWavenumber, "/cm");
    print("townsend eta coeff (red)"    , "η/N" , townsendEta.first/density, optional<QtySiArea>(townsendEta.second/density), unitArea, "cm²");
    cout << endl;
    print("diffusion coeff"             , "D"   , diffusionCoeff.first, optional<QtySiDiffusion>(diffusionCoeff.second), unitDiffusion, "cm²/s");
    print("diffusion coeff (norm)"      , "ND"  , diffusionCoeff.first * density, optional<QtySiNormalizedDiffusion>(diffusionCoeff.second * density), unitNormalizedDiffusion, "/cm⋅s");
    print("diffusion coeff (ratio)"     , "D/μ" , diffusionCoeffRatio.first, optional<QtySiElectricPotential>(diffusionCoeffRatio.second), unitElectricPotential, "V");
    print("diffusion coeff long"        , "Dᴸ"  , diffusionCoeffL.first, optional<QtySiDiffusion>(diffusionCoeffL.second), unitDiffusion, "cm²/s");
    print("diffusion coeff long (norm)" , "NDᴸ" , diffusionCoeffL.first * density, optional<QtySiNormalizedDiffusion>(diffusionCoeffL.second * density), unitNormalizedDiffusion, "/cm⋅s");
    print("diffusion coeff long (ratio)", "Dᴸ/μ", diffusionCoeffRatioL.first, optional<QtySiElectricPotential>(diffusionCoeffRatioL.second), unitElectricPotential, "V");
    print("diffusion coeff tran"        , "Dᵀ"  , diffusionCoeffT.first, optional<QtySiDiffusion>(diffusionCoeffT.second), unitDiffusion, "cm²/s");
    print("diffusion coeff tran (norm)" , "NDᵀ" , diffusionCoeffT.first * density, optional<QtySiNormalizedDiffusion>(diffusionCoeffT.second * density), unitNormalizedDiffusion, "/cm⋅s");
    print("diffusion coeff tran (ratio)", "Dᵀ/μ", diffusionCoeffRatioT.first, optional<QtySiElectricPotential>(diffusionCoeffRatioT.second), unitElectricPotential, "V");
    print("diffusion magnitude"         , "σ"   , diffusionMagnitude.first, optional<QtySiLength>(diffusionMagnitude.second),   unitLength, "cm");
    print("diffusion magnitude long"    , "σᴸ"  , diffusionMagnitudeL.first, optional<QtySiLength>(diffusionMagnitudeL.second), unitLength, "cm");
    print("diffusion magnitude tran"    , "σᵀ"  , diffusionMagnitudeT.first, optional<QtySiLength>(diffusionMagnitudeT.second), unitLength, "cm");

    cout << endl;
    print("real collisions", "", QtySiDimensionless( stats.realCollisions), optional<QtySiDimensionless>(), QtySiDimensionless(1.), "");
    print("null collisions", "", QtySiDimensionless( stats.nullCollisions), optional<QtySiDimensionless>(), QtySiDimensionless(1.), "");
    print("fail collisions", "", QtySiDimensionless( stats.failCollisions), optional<QtySiDimensionless>(), QtySiDimensionless(1.), "");
    print("efficiency",      "", QtySiDimensionless( stats.getEfficency() * 100), optional<QtySiDimensionless>(), QtySiDimensionless(1.), "%");
    print("elapsed time",    "", QtySiTime ( duration * si::second), optional<QtySiTime>(), QtySiTime (1. * si::second), "s");

}

