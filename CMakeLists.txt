cmake_minimum_required(VERSION 3.0)
project(drifter VERSION 1.0.0 LANGUAGES CXX)
include(GNUInstallDirs)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Wno-unused-parameter" )

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    add_compile_options(-fconcepts)
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang")
    add_compile_options(-fdeclspec -Wno-pre-c++2b-compat)
endif ()

set(BUILD_MARCH "" CACHE STRING "Set the compiler -march flag: (empty), generic, native, ...")
if (NOT BUILD_MARCH STREQUAL "")
        add_compile_options("-march=${BUILD_MARCH}")
endif()

include_directories(${PROJECT_SOURCE_DIR}/external/CLI11/include)
include_directories(${PROJECT_SOURCE_DIR}/include)
add_subdirectory(${PROJECT_SOURCE_DIR}/external/backward-cpp)

file(GLOB SOURCE_FILES ${PROJECT_SOURCE_DIR}/src/main.cpp)

add_executable(drifter ${SOURCE_FILES} ${BACKWARD_ENABLE})

if(NOT EXISTS "${PROJECT_SOURCE_DIR}/external/backward-cpp/backward.hpp")
    message(FATAL_ERROR "Unable to find the directory 'external/backward-cpp/backward.hpp'.
Did you perform?
  git submodule init
  git submodule update")
endif()

add_backward(drifter)

find_package(Betaboltz REQUIRED)
target_link_libraries(drifter DFPE::betaboltz)

# Exporting targets

install(TARGETS drifter RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR})

if(NOT EXISTS "${PROJECT_SOURCE_DIR}/external/CLI11/include")
   message(FATAL_ERROR "Unable to find the directory 'external/CLI11/include'.
Did you perform?
  git submodule init
  git submodule update")
endif()

